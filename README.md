#  WundChecker Interview App

## Description
- Upon opening the app, a list of location is loaded from the backend via an API

- Clicking on any item on the list navigates the app to the Mapview

- The Map View displays the selected item on the map and and info about it on the marker

## Third Party Libraries:
- Alamofire: For Networking Calls.

- EvReflection: For Json Deserialization.

- RxSwift: For reactive programming.

- RxCocoa: For UI binding.

- Google Maps: For map views.

- JGProgressHUD: For progress bar.

## App Architecture:

- MVVM C
  - Enhances Testability.
  - Follows Single responsibility principle.
  - Keeps the code maintainable.
  
## Improvements:
  
 -  Add  Tests
 
 - Handling Edge case scenarios e.g when there's no internet.

-  Improve User Interface Design 

-  Implement Coordinator.
