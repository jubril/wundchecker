//
//  ViewController.swift
//  WunderChecker
//
//  Created by Jubril   on 1/29/20.
//  Copyright © 2020 Jubril. All rights reserved.
//

import UIKit
import JGProgressHUD
import RxSwift

// TODO: Implement Cordinator to complete the Architecture


class LocationListViewController: UIViewController {
  private var viewModel: LocationListViewModel!
  let hud = JGProgressHUD(style: .dark)
  let disposeBag = DisposeBag()
  let cellIdentifier = "IndividualPlacemarksCellView"
  @IBOutlet weak var locationListTableView: UITableView!

    
  override func viewDidLoad() {
    super.viewDidLoad()
    let nib = UINib(nibName: cellIdentifier, bundle: nil)
    locationListTableView.register(nib, forCellReuseIdentifier: cellIdentifier)
    locationListTableView.rowHeight = 240
    viewModel = LocationListViewModel(APIService())
    viewModel.getLocationData()
    setUpListenerForTableviewData()
    setUpListenerForNetworkCallProgress()
    setUpListenerForNetworkError()
    setUpClickListenerForTableViewCell()
  }
    
    
    func setUpListenerForTableviewData(){
        viewModel.locationDataResponseObservable.asObservable().bind(to: locationListTableView.rx.items(cellIdentifier: self.cellIdentifier,cellType: IndividualPlacemarksCellView.self)){ row, model, cell in
           cell.locationItem = model
         }.disposed(by: disposeBag)
    }
    
    func setUpListenerForNetworkCallProgress() {
        viewModel.isLoadingObservable.subscribe(onNext: { loading in
             if loading {
                UIUtil.shared.showProgressBar(message: StringConstant.Loading, view: self.view)
             } else {
                UIUtil.shared.dismissProgressBar()
             }
         }).disposed(by: disposeBag)
    }
    
    func setUpListenerForNetworkError(){
        viewModel.errorMessageObservable.subscribe(onNext: { message in
        if !message.isEmpty {
            UIUtil.shared.displayMessage(messsage: message, controller: self)
        }
        }).disposed(by: disposeBag)
    }
    
    func setUpClickListenerForTableViewCell(){
        locationListTableView.rx
                   .modelSelected(LocationResponse.self)
                   .subscribe(onNext:  { value in
                      let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(identifier: "MapController") as MapViewController
                           vc.locationDataResponseObservable = self.viewModel.locationDataResponseObservable
                      vc.selectedLocation = value
                           self.navigationController?.pushViewController(vc, animated: true)
                   })
                   .disposed(by: disposeBag)
    }

}

