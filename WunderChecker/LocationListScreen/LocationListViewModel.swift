//
//  LocationListViewModel.swift
//  WunderChecker
//
//  Created by Jubril   on 2/1/20.
//  Copyright © 2020 Jubril. All rights reserved.
//

import Foundation
import RxSwift
import RxCocoa

// TODO: Add tests 



protocol LocationListViewModelProtocol {
    func getLocationData()
}


class LocationListViewModel: LocationListViewModelProtocol {
  var service: APIService
  let locationDataRelay = BehaviorRelay<[LocationResponse]>(value: [])
  let isLoadingRelay = BehaviorRelay(value: false)
  let errorMessageRelay = BehaviorRelay(value: "")
  let disposeBag = DisposeBag()

  var locationDataResponseObservable: Observable<[LocationResponse]> {
         return locationDataRelay.asObservable()
     }

  var isLoadingObservable: Observable<Bool> {
         return isLoadingRelay.asObservable()
  }

  var errorMessageObservable: Observable<String> {
         return errorMessageRelay.asObservable()
     }

  init(_ service: APIService) {
    self.service = service
  }

  func getLocationData(){
    isLoadingRelay.accept(true)
    service.getLocationData().subscribe({ event in
        self.isLoadingRelay.accept(false)
      switch event {
               case .next(let locationList):
                 self.locationDataRelay.accept(locationList)
               case .error(let error):
                 self.errorMessageRelay.accept(error.localizedDescription)
               case .completed:
                 self.errorMessageRelay.accept("No Response from Server")
               }
      }).disposed(by: disposeBag)
  }


}
