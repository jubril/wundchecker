//
//  APIService.swift
//  WunderChecker
//
//  Created by Jubril   on 1/29/20.
//  Copyright © 2020 Jubril. All rights reserved.
//

import Foundation
import RxSwift
import Alamofire


protocol APIServiceProtocol {
    func getLocationData() -> Observable<[LocationResponse]>
}


class APIService: APIServiceProtocol {
  let API_URL = "https://wunder-test-case.s3-eu-west-1.amazonaws.com/ios/locations.json"

  func getLocationData() -> Observable<[LocationResponse]> {
    return Observable<[LocationResponse]>.create{ (observer) -> Disposable in
      Alamofire.request(self.API_URL, method: .get,encoding: JSONEncoding.default)
        .responseObject { (response: DataResponse<LocationListResponse>) in
        switch response.result {
        case .success(_):
          if let newestResponse = response.value?.placemarks {
            observer.onNext(newestResponse)
          } else {
            observer.onCompleted()
          }
        case .failure(let error):
          observer.onError(error)
          }
      }
      return Disposables.create()
    }

  }
}
