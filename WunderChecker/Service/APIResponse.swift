//
//  APIResponse.swift
//  WunderChecker
//
//  Created by Jubril   on 1/29/20.
//  Copyright © 2020 Jubril. All rights reserved.
//

import Foundation
import EVReflection

@objc(LocationResponse)
class LocationResponse: EVNetworkingObject {
  var address: String?
  var engineType: String?
  var coordinates:[Double]?
  var exterior: String?
  var fuel: Int?
  var interior: String?
  var name: String?
  var vin: String?
}


@objc(LocationListResponse)
class LocationListResponse: EVNetworkingObject {
  var placemarks:[LocationResponse]?
}


