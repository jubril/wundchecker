//
//  UIUtil.swift
//  WunderChecker
//
//  Created by Jubril Edu  on 03/02/2020.
//  Copyright © 2020 Jubril. All rights reserved.
//

import Foundation
import JGProgressHUD

class UIUtil {
    static let shared = UIUtil()
    let hud = JGProgressHUD(style: .dark)
    
    private init () {
    }
    
    func showProgressBar(message: String?,view: UIView){
      if !hud.isVisible {
        hud.textLabel.text = message
        hud.show(in: view)
      }
    }

    func dismissProgressBar(){
      if hud.isVisible {
        hud.dismiss()
      }
    }
      
    func displayMessage(messsage: String, controller: UIViewController) {
      let alert = UIAlertController(title: StringConstant.Title, message: messsage, preferredStyle: .alert)
      alert.addAction(UIAlertAction(title: StringConstant.Okay, style: .default, handler: nil))
      controller.present(alert, animated: true, completion: nil)
      }
    
}
