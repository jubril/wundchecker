//
//  IndividualPlacemarksCellView.swift
//  WunderChecker
//
//  Created by Jubril   on 1/29/20.
//  Copyright © 2020 Jubril. All rights reserved.
//

import Foundation
import UIKit

class IndividualPlacemarksCellView: UITableViewCell {

  @IBOutlet weak var nameLabel: UILabel!
  @IBOutlet weak var addressLabel: UILabel!
  @IBOutlet weak var engineTypeLabel: UILabel!
  @IBOutlet weak var interiorLabel: UILabel!
  @IBOutlet weak var exteriorLabel: UILabel!
  @IBOutlet weak var fuelLabel: UILabel!
  @IBOutlet weak var VinLabel: UILabel!
  var locationItem: LocationResponse? {
    didSet{
      guard let item = locationItem else { return }
      nameLabel.text = item.name
      addressLabel.text = item.address
      engineTypeLabel.text = item.engineType
      interiorLabel.text = item.interior
      exteriorLabel.text = item.exterior
      fuelLabel.text = String(describing: item.fuel ?? 0)
      VinLabel.text = item.vin
    }
  }


  override func awakeFromNib() {
    super.awakeFromNib()
  }
}
