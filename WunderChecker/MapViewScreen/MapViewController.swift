//
//  MapViewController.swift
//  WunderChecker
//
//  Created by Jubril Edu  on 02/02/2020.
//  Copyright © 2020 Jubril. All rights reserved.
//

import Foundation
import UIKit
import GoogleMaps
import RxSwift

class MapViewController: UIViewController {
    
    var locationData: [LocationResponse]!
    var selectedLocation: LocationResponse!
    var locationDataResponseObservable: Observable<[LocationResponse]>!
    let disposeBag = DisposeBag()
    
    
    override func loadView() {
        locationDataResponseObservable.asObservable().subscribe({ result in
            let cameraStartingPoint = GMSCameraPosition.camera(withLatitude: result.element![0].coordinates![1], longitude: result.element![0].coordinates![0], zoom: 14.0)
            let mapView = GMSMapView.map(withFrame: CGRect.zero, camera: cameraStartingPoint)
            self.view = mapView
            for location in result.element! {
                   let marker = GMSMarker()
                   marker.position = CLLocationCoordinate2D(latitude: location.coordinates![1], longitude: location.coordinates![0])
                   marker.title = location.name
                marker.icon = UIImage(named: "marker")
                   marker.snippet = location.address
                marker.map = mapView

                if location == self.selectedLocation {
                    mapView.selectedMarker = marker
                }
               }
            mapView.animate(to: GMSCameraPosition.camera(withLatitude: self.selectedLocation.coordinates![1], longitude: self.selectedLocation.coordinates![0], zoom: 17.0))
            
            }).disposed(by: disposeBag)
       
    }
}
