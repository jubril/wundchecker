//
//  StringConstant.swift
//  WunderChecker
//
//  Created by Jubril Edu  on 03/02/2020.
//  Copyright © 2020 Jubril. All rights reserved.
//

import Foundation

struct StringConstant {
    static let Loading = "Loading"
    static let Title = "Message"
    static let Okay = "Okay"
}
